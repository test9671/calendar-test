import { TaskMap } from '../types';

export const exportToJSON = (taskMap: TaskMap) => () => {
  const jsonString = `data:text/json;chatset=utf-8,${encodeURIComponent(
    JSON.stringify(taskMap)
  )}`;
  const link = document.createElement('a');
  link.href = jsonString;
  link.download = 'calendar.json';

  link.click();
};
