import { Color, Tag, TaskMap } from '../types';

export const filterBySearch = (map: TaskMap, searchValue: string) => {
  if (!searchValue) return map;
  const filtered = Object.entries(map).reduce((acc: TaskMap, [k, tasks]) => {
    acc[k] = tasks.filter((task) =>
      task.title.toLowerCase().includes(searchValue.toLowerCase())
    );
    return acc;
  }, {});
  return filtered;
};

export const filterByColor = (map: TaskMap, color: Color | undefined) => {
  if (!color) return map;
  const filtered = Object.entries(map).reduce((acc: TaskMap, [k, tasks]) => {
    acc[k] = tasks.filter((task) => task.color?.includes(color));
    return acc;
  }, {});
  return filtered;
};

export const filterByTag = (map: TaskMap, tag: Tag | undefined) => {
  if (!tag) return map;
  const filtered = Object.entries(map).reduce((acc: TaskMap, [k, tasks]) => {
    acc[k] = tasks.filter((task) => task.tag?.includes(tag));
    return acc;
  }, {});
  return filtered;
};
