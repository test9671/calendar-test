import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import { DragDropContext } from 'react-beautiful-dnd';

import { Color, Holidays, Tag, Task, TaskMap } from './types';
import { useModal } from './hooks/useModal';
import { useFetch } from './hooks/useFetchHolidays';
import { filterBySearch, filterByColor, filterByTag } from './utils/filters';
import { reorderTasks } from './utils/reorder';
import { exportAsImage } from './utils/exportAsImage';
import { convertHolyToMap } from './utils/convertHolyToMap';
import { exportToJSON } from './utils/exportToJSON';
import { API_URL } from './constants';
import { Modal } from './components/Modal';
import { TaskList } from './TaskList';
import {
  ActionPanel,
  FilterPanel,
  Form,
  FormFooter,
  Input,
  Layout,
  Select,
  TextArea,
} from './app.style';

const initialTaskValues: Task = {
  id: '',
  title: '',
  description: '',
};

const App = () => {
  const { response, error, isLoading } = useFetch<Holidays[]>(API_URL);

  const { isShown, toggle } = useModal();
  const exportRef = useRef<HTMLDivElement>(null);

  const [taskMap, setTasks] = useState<TaskMap>({});
  const [monthDay, setMonthDay] = useState('');
  const [values, setActiveTaskValues] = useState(initialTaskValues);
  const [search, setSearch] = useState('');
  const [color, setColor] = useState<Color>();
  const [tag, setTag] = useState<Tag>();
  const [filteredTaskMap, setFilteredTaskMap] = useState<TaskMap>();

  useEffect(() => {
    if (response) {
      const map = convertHolyToMap(response);
      setTasks(map);
    }
  }, [response]);

  useEffect(() => {
    const bySearch = filterBySearch(taskMap, search);
    const byColor = filterByColor(bySearch, color);
    const byTag = filterByTag(byColor, tag);

    setFilteredTaskMap(byTag);
  }, [color, search, tag, taskMap]);

  const reset = useCallback(() => {
    setActiveTaskValues(initialTaskValues);
    setMonthDay('');
  }, []);

  const handleAdd = useCallback(
    (key: string, task: Task) => {
      setTasks({ ...taskMap, [key]: [...taskMap[key], task] });
    },
    [taskMap]
  );

  const handleEdit = useCallback(
    (key: string, task: Task) => {
      setTasks({
        ...taskMap,
        [key]: taskMap[key].map((t) => (t.id === task.id ? task : t)),
      });
    },
    [taskMap]
  );

  const handleChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
      const name = e.target.name;
      const value = e.target.value;
      setActiveTaskValues({ ...values, [name]: value });
    },
    [values]
  );

  const handleMultiSelect = useCallback(
    (e: React.ChangeEvent<HTMLSelectElement>) => {
      const options = e.target.selectedOptions;
      const name = e.target.name;

      const optionsArray = Array.from(options).map((o) => o.value);
      setActiveTaskValues({ ...values, [name]: optionsArray });
    },
    [values]
  );

  const handleCancel = useCallback(
    (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
      e.preventDefault();
      toggle();
      reset();
    },
    [reset, toggle]
  );

  const handleSubmit = useCallback(
    (e: React.FormEvent<HTMLFormElement>) => {
      e.preventDefault();
      if (values.id) {
        handleEdit(monthDay, values);
        toggle();
        reset();
      } else if (values.title && values.description) {
        const task: Task = {
          id: ((Math.random() + 1) * 10).toString(36),
          title: values.title,
          description: values.description,
          color: values.color || [],
          tag: values.tag || [],
        };

        handleAdd(monthDay, task);
        toggle();
        reset();
      }
    },
    [handleAdd, handleEdit, monthDay, reset, toggle, values]
  );

  const content = useMemo(
    () => (
      <Form onSubmit={handleSubmit}>
        <div>
          <div>
            <label htmlFor='title'>Title</label>
          </div>
          <Input
            type='text'
            name='title'
            id='title'
            value={values.title}
            onChange={handleChange}
          />
        </div>
        <div>
          <div>
            <label htmlFor='description'>Description</label>
          </div>
          <TextArea
            name='description'
            id='description'
            value={values.description}
            onChange={handleChange}
          />
        </div>
        <div>
          <div>
            <label htmlFor='color'>Color: </label>
          </div>
          <Select name='color' id='color' multiple onChange={handleMultiSelect}>
            <option value=''>--Please choose color(s)--</option>
            <option value='red'>Red</option>
            <option value='blue'>Blue</option>
            <option value='green'>Green</option>
          </Select>
        </div>
        <div>
          <div>
            <label htmlFor='tag'>Tag: </label>
          </div>
          <Select
            name='tag'
            id='tag'
            multiple
            size={3}
            onChange={handleMultiSelect}
          >
            <option value=''>--Please choose tag(s)--</option>
            <option value='feature'>Feature</option>
            <option value='bug'>Bug</option>
          </Select>
        </div>
        <FormFooter>
          <button type='reset' onClick={handleCancel}>
            Cancel
          </button>
          <button
            type='submit'
            disabled={!(values.title && values.description)}
          >
            {values.id ? 'Update' : 'Create'}
          </button>
        </FormFooter>
      </Form>
    ),
    [
      handleCancel,
      handleChange,
      handleMultiSelect,
      handleSubmit,
      values.description,
      values.id,
      values.title,
    ]
  );

  const handleUpload = useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
    const fileReader = new FileReader();
    if (e.target.files) {
      fileReader.readAsText(e.target.files[0], 'UTF-8');
    }
    fileReader.onloadend = () => {
      if (fileReader.result) {
        const strFiles = fileReader.result.toString();
        const map = JSON.parse(strFiles) as TaskMap;
        setTasks(map);
      }
    };
  }, []);

  const handleHide = useCallback(() => {
    toggle();
    reset();
  }, [reset, toggle]);

  if (isLoading) return <p>Loading...</p>;
  if (error) return <p>Something went wrong...</p>;

  return (
    <Layout>
      <FilterPanel>
        <div>
          <label htmlFor='search'>Search</label>
          <input
            type='search'
            name='search'
            id='search'
            onChange={(e) => setSearch(e.target.value)}
          />
        </div>
        <div>
          <label htmlFor='color'>Color: </label>
          <select
            name='color'
            id='color'
            onChange={(e) => setColor(e.target.value as Color)}
          >
            <option value=''>--Please choose color(s)--</option>
            <option value='red'>Red</option>
            <option value='blue'>Blue</option>
            <option value='green'>Green</option>
          </select>
        </div>
        <div>
          <label htmlFor='tag'>Tag: </label>
          <select
            name='tag'
            id='tag'
            onChange={(e) => setTag(e.target.value as Tag)}
          >
            <option value=''>--Please choose tag(s)--</option>
            <option value='feature'>Feature</option>
            <option value='bug'>Bug</option>
          </select>
        </div>
      </FilterPanel>
      <DragDropContext
        onDragEnd={({ destination, source }) => {
          let isFixed;
          if (destination) {
            isFixed =
              filteredTaskMap?.[destination?.droppableId]?.[destination.index]
                ?.fixed;
          }

          if (!destination || isFixed) {
            return;
          }

          setTasks(reorderTasks(taskMap, source, destination));
        }}
      >
        <div ref={exportRef}>
          {filteredTaskMap &&
            Object.entries(filteredTaskMap).map(([k, v]) => (
              <TaskList
                internalScroll
                key={k}
                listId={k}
                listType='CARD'
                tasks={v}
                onToggle={toggle}
                setMonthDay={setMonthDay}
                setActiveTaskValues={setActiveTaskValues}
              />
            ))}
        </div>
      </DragDropContext>
      <ActionPanel>
        <button type='button' onClick={exportToJSON(taskMap)}>
          Export Calendar to JSON
        </button>
        <button
          onClick={() => {
            exportAsImage(exportRef.current, 'calendar');
          }}
        >
          Export Calendar as Image
        </button>
        <div>
          <h3>Upload Json</h3>
          <input
            type='file'
            accept='application/json'
            onChange={handleUpload}
          />
        </div>
      </ActionPanel>
      <Modal
        isShown={isShown}
        hide={handleHide}
        modalContent={content}
        headerText={values.id ? 'Edit Task' : 'Create Task'}
      />
    </Layout>
  );
};

export default App;
