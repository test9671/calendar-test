import React, { FC, memo } from 'react';
import {
  CardTitle,
  CardBody,
  CardDescription,
  ColorLabel,
  TagLabel,
  EditButton,
} from './card.style';
import { ShowModal, Task } from '../../types';

type Props = {
  task: Task;
  showModal: ShowModal;
};
export const Card: FC<Props> = memo(({ task, showModal }) => {
  return (
    <>
      <CardTitle>{task.title}</CardTitle>
      <CardBody color={task.fixed ? 'pink' : 'lightblue'}>
        <CardDescription>{task.description}</CardDescription>
        <div>
          {task.color?.map((color) => (
            <ColorLabel key={color} color={color} />
          ))}
        </div>
        <div>
          {task.tag?.map((tag) => (
            <TagLabel key={tag}>{tag}</TagLabel>
          ))}
        </div>
        {!task.fixed && (
          <EditButton onClick={() => showModal(task)}>Edit</EditButton>
        )}
      </CardBody>
    </>
  );
});

export default Card;
