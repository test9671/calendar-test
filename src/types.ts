export type Color = 'red' | 'blue' | 'green';
export type Tag = 'feature' | 'bug';
export interface Task {
  id: string;
  title: string;
  description: string;
  color?: Array<Color>;
  tag?: Array<Tag>;
  fixed?: boolean;
}

export type TaskMap = { [key: string]: Task[] };

export interface Holidays {
  date: string;
  localName: string;
  name: string;
  countryCode: string;
  fixed: boolean;
  global: boolean;
  counties: [string] | null;
  launchYear: number | null;
  types: ['Public'];
}

export type ShowModal = (task?: Task) => void;
